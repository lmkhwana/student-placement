from flask import Flask, render_template, request, redirect
from forms import SignonForm
import sqlite3
from flask import g

app = Flask(__name__)
app.config['SECRET_KEY'] = 'you-will-never-guess'

@app.route('/asnejds2uy29734h34nrf40n3m348umf43mc9348493m', methods=['GET'])
def dashboard():
    con = sqlite3.connect("database.db")
    print('success')
    con.row_factory = sqlite3.Row
    print('success')

    cur = con.cursor()
    print('success')

    cur.execute("select  * from students")
    print('success')


    rows = cur.fetchall();
    print('success')

    return render_template('dashboard.html', rows=rows)


@app.route('/login', methods=['POST'])
def login():
    if request.method == 'POST':
        user = request.form['username']
        password = request.form['password']
        print(user)

        if user == 'StudentPlacement' and password == '20200418':
            return redirect('/asnejds2uy29734h34nrf40n3m348umf43mc9348493m')
        else:
            return redirect('/')



@app.route('/', methods=['GET', 'POST'])
def home():
    form = SignonForm()

    if request.method == 'POST':
        try:
            name = form.name.data
            lastname = form.lastname.data
            gender = form.gender.data
            idnumber = form.idnumber.data
            cell = form.cell.data
            email = form.email.data
            school = form.school.data
            university = form.university.data
            choice = form.choice.data
            sponsor = form.sponsor.data

            print('success')
            with sqlite3.connect("database.db") as con:
                cur = con.cursor()
                print('success')

                cur.execute("INSERT INTO students (first_name,last_name,gender,id_number,"
                            "cell_number,email,current_school, university_of_choice, choise_of_study,"
                            "financial_sponsor) VALUES(?,?,?,?,?,?,?,?,?,?)",
                            (name, lastname, gender, idnumber, cell, email, school,
                             university, choice, sponsor))
                print('success')

                con.commit()
                print('success')

                msg = "Record successfully added"
                print(msg)
        except Exception as e:
            con.rollback()
            print(str(e))

    return render_template('home.html', form=form)


if __name__ == '__main__':
    app.run()
