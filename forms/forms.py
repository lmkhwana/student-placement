from flask_wtf import FlaskForm
from wtforms import StringField, TextField, SubmitField
from wtforms.validators import DataRequired, Length

class SignonForm(FlaskForm):
    name = StringField('First Name', validators=[DataRequired()],render_kw={"placeholder": "test"})
    lastname = StringField('Last Name', validators=[DataRequired()])
    gender = StringField('Gender', validators=[DataRequired()])
    idnumber = StringField('ID Number/Student number', validators=[DataRequired()])
    cell = StringField('Cell Number', validators=[DataRequired()])
    email = StringField('Email Address', validators=[DataRequired()])
    school = StringField('Current School/University', validators=[DataRequired()])
    university = StringField('University Of Choice', validators=[DataRequired()])
    choice = StringField('Choice Of Study', validators=[DataRequired()])
    sponsor = StringField('Financial Sponsor', validators=[DataRequired()])

    submit = SubmitField('Register')